# mqtt2docker

[![](https://images.microbadger.com/badges/image/homesmarthome/mqtt2docker.svg)](https://microbadger.com/images/homesmarthome/mqtt2docker)

[![](https://img.shields.io/badge/dynamic/json.svg?label=version&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%mqtt2docker%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4)]("https://hub.docker.com/r/homesmarthome/mqtt2docker/")

MQTT subscriber which transforms mqtt messages to Docker Run commands.

Format of message:
```
docker/run/123-456-789/busybox
```
will be transformed to execute the following command:
```
docker run busybox
```
and sends the following message with the output as payload:
```
docker/status/123-456-789/busybox
```
Last part of the message is the docker image and can also include namespace and tag.
If the run command needs some additional parameters, these can be added to the run command by sending a payload in the following format:
```
{
  "params": "ls -la"
}
```
If the container needs one or multiple environments vars, these can be added by sending a payload in the following format:
```
{
  "env": [
    {"name1": "val1"},
    {"name2": "val2"}
  ]
}
```

Needs `MQTT_HOST` var in env config.

## Install as a Service with Portainer
1. Open portainer ([homesmarthome.local:9000](http://homesmarthome.local:9000))
2. Click on *Services* in the menu
3. Click on *+ Add Service*
4. Configuration:

   * Name: mqtt2docker
   * Image configuration
     * Name: ![](https://img.shields.io/badge/dynamic/json.svg?label=Image&url=https%3A%2F%2Fregistry.hub.docker.com%2Fv2%2Frepositories%2Fhomesmarthome%mqtt2docker%2Ftags&query=%24.results%5B1%5D.name&colorB=4286f4&prefix=homesmarthome/mqtt2docker:&style=flat-square)
   * Volumes (map additinal volume)
     * container: `/env` (`Bind`)
     * host: `/config/env`
   * Volumes (map additinal volume)
     * container: `/var/run/docker.sock` (`Bind`)
     * host: `/var/run/docker.sock`
     
5. Actions
    * `Create the service`
