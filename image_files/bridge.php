<?php

$c = new Mosquitto\Client;

$c->onConnect(function() use ($c) {
    echo "Connected to MQTT broker\n";
});

$c->onMessage(function($message) use ($c) {
    echo "Received message\n";

    $tokens = explode('/', $message->topic);
    array_shift($tokens); array_shift($tokens);
    $uuid = array_shift($tokens);
    $dockerImage = join('/', $tokens);

    $params = '';
    if ($payload = @json_decode($message->payload, true)) {
        $params = $payload['params'];
        $payloadEnv = $payload['env'];

        $env = '';
        if (is_array($payloadEnv)) {
            foreach ($payloadEnv as $e) {
                $env .= ' -e ' . key($e) . '=' . current($e);
            }
        }
    };

    $result = shell_exec ("docker run $env $dockerImage $params"); 

    var_dump("docker run $env $dockerImage $params");

    $c->publish("docker/status/$uuid/$dockerImage", $result, 0);
});

sleep(5);

$c->connect(getenv('MQTT_HOST'));

$c->subscribe('docker/run/#', 1);

$c->loopForever();
echo "Finished\n";
