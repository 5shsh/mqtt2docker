<?php

$config = json_decode(require '/config.json', true);

$c = new class ($config) extends Mosquitto\Client {
    private $config;

    public function __construct($config) {
        $this->config = $config;
        parent::__construct();
    }

    public function getConfig() {
        return $this->config;
    }
};

$c->onConnect(function() use ($c) {
    echo "Connected to MQTT broker\n";
});

$c->onMessage(function($message) use ($c) {
    echo "Received message\n";

    $tokens = explode($message->topic);
    array_shift(); array_shift();
    $uuid = array_shift();
    $dockerImage = join($tokens, '/');

    $params = '';
    if ($payload = @json_decode($message->payload, true)) {
        $params = $payload['params'];
    };

    $result = shell_exec ("docker run $dockerImage $params"); 

    $c->publish("docker/status/$uuid/$dockerImage", $result, 0);
});

sleep(5);

$c->connect('mosquitto');

$c->subscribe('docker/get/#', 1);

$c->loopForever();
echo "Finished\n";
