docker_run:
	docker run -d --name=mosquitto_test_run homesmarthome/mosquitto:latest
	docker run -d \
	  --name=mqtt2docker_test_run \
	  -v $(PWD)/test/env:/env \
		-v /var/run/docker.sock:/var/run/docker.sock \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep mqtt2docker_test_run

docker_stop:
	docker rm -f mqtt2docker_test_run 2> /dev/null ; true
	docker rm -f mosquitto_test_run 2> /dev/null; true
